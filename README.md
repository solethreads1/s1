
Amazing women are around us. At Solethreads, we’re lucky enough to have many of those women working in all areas of the company, with a great amount in leadership roles. Within our ever-growing appreciation, we want to celebrate the women who make Solethreads what it is by highlighting their stories, achievements and more with this Women of Solethreads blog series.

Lisa Fuentes is the Director of Customer Care at Solethreads. Now in her 7th year in the Solethreads family, she's made a lasting impact as a trailblazing leader at the company. With fearlessness, determination, and humour, she continues to pave just how for others while always remaining true to what matters most in her career and family life. Read her Q&A, and find out about our Women of Solethreads spotlight new slippers of the week!
<a href="https://www.solethreads.com/collections/stylish-flip-flops">look here</a>


How long have you been with Solethreads?
Seven long, glorious, and educational years.

 

What’s your title role at Solethreads, and what does that entail?
My current role at Solethreadsis Director of Customer Care. I manage various teams, and I manage the wholesale order book of business, made up of Standard Accounts, Key Accounts and International Accounts. Along with that, our Returns Department falls under the CUSTOMER SUPPORT umbrella. I acquired the Wholesale Returns Department in 2015 so that we're able to standardize and increase the entire wholesale order process from start to finish. The bottom line is, my goal is and always will be to create a positive customer experience.

 

Why did you decide to work at Solethreads?
I left a long career in the mortgage business and was looking for stability at a time when the economy and housing market were very volatile. Solethreadsafforded me a place where I could use my skill set to grow professionally. Coming in when Solethreads(formerly Vasyli) was in its infancy, was ideal and gave me the chance to grow an interior team to fit the exponential growth we have experienced through the years.

One of the first new look slippers we did as a company was to identify our core values. Instead of having management plastering values on a wall, they empowered the employees to identify what core values were important to us. This finally meant that we held ourselves accountable to our values! I knew from then on team building experience Solethreads would be a career.

 

What do you feel has been your greatest accomplishment at Solethreads?<a href="https://goo.gl/maps/UznSdr73tZ35kmsC9">see post</a>
My greatest accomplishment is my customer care team. When I was the customer support supervisor in 2011, I had three team members. Over the years, my team is continuing to grow into a self-sustaining department. Training employees from the ground up has enabled them to take a motivated stake in owning their “piece” of Solethreads. I am proud to share that many of my former team members have been promoted to other parts of the business enterprise and made their initial tenure with Solethreads a career.

 

How does Solethreads empower women?
Professionally, Solethreads empowers women who are trying to find something more than simply a “9 to 5” job. The hands-on training and culture have been verified to be a perfect recipe for success. The task environment is fast-paced yet rewarding. Many persons attended to Solethreads and made this normally a career rather than just a job.

From a consumer perspective, Solethreads empowers women to get on their feet! A lot of times, persons take for granted the ability to walk around, pain-free. Solethreads has helped millions of girls start walking and living again while looking stylish.

 

What are 3 things that are important for you when working on a team?
Communication, appreciation and leadership.

 

What advice do you have for young women starting in the workforce?
Work hard, support your team, stay hungry. Your success will come. Oh, and take notes!



Where do you see the future of Solethreads heading?
The sky is the limit. When I walked in 7 years ago, I could not have imagined this sort of success. Changing people’s lives will continually be at the forefront of what we do at Solethreads.

 

Do you have a favourite Solethreads style?
Our Satima Sneaker and Tide II Sandal are SOLID styles that have held the test of time. I would back up the truck on these, any day of the week. (That’s a shipping joke!)

What’s your secret to feeling your best self?
Sleep! I’m a Director at Solthreads and a mom, and there are a lot of tasks that come with those titles. I often feel like I can “sleep later,” but it will eventually catch up with you. I must carve out time for myself to meditate and get enough sleeping. I am an improved version of myself for my team and my daughters.

अद्भुत महिलाएं हमारे आसपास हैं । सोलेथ्रेड्स में, हम भाग्यशाली हैं कि उन महिलाओं में से कई कंपनी के सभी क्षेत्रों में काम कर रही हैं, जिनमें नेतृत्व की भूमिकाओं में एक बड़ी राशि है । हमारी लगातार बढ़ती प्रशंसा के भीतर, हम उन महिलाओं का जश्न मनाना चाहते हैं जो सोलेथ्रेड्स ब्लॉग श्रृंखला की इस महिलाओं के साथ अपनी कहानियों, उपलब्धियों और अधिक को उजागर करके सोलेथ्रेड्स बनाती हैं । 



लिसा फ्यूएंट्स सोलेथ्रेड में कस्टमर केयर की निदेशक हैं । अब सोलेथ्रेड्स परिवार में अपने 7 वें वर्ष में, उन्होंने कंपनी में एक ट्रेलब्लैजिंग लीडर के रूप में एक स्थायी प्रभाव डाला है । निडरता, दृढ़ संकल्प और हास्य के साथ, वह दूसरों के लिए बस कैसे प्रशस्त करती रहती है, जबकि हमेशा अपने करियर और पारिवारिक जीवन में सबसे ज्यादा मायने रखती है । उसके क्यू एंड ए पढ़ें, और हमारे बारे में पता करें सोलेथ्रेड की महिलाएं सप्ताह की स्पॉटलाइट!







आप एकमात्र के साथ कब तक रहे हैंधीरे?

सात लंबे, शानदार और शैक्षिक वर्ष।



 



सोलेथ्रेड में आपकी शीर्षक भूमिका क्या है, और यह क्या करता है?

ग्राहक सेवा के सोलेथ्रेडिस निदेशक में मेरी वर्तमान भूमिका। मैं विभिन्न टीमों का प्रबंधन करता हूं, और मैं व्यापार की थोक ऑर्डर बुक का प्रबंधन करता हूं, जो मानक खातों, प्रमुख खातों और अंतर्राष्ट्रीय खातों से बना है । इसके साथ ही, हमारा रिटर्न विभाग ग्राहक सहायता की छतरी के नीचे आता है । मैंने 2015 में थोक रिटर्न विभाग का अधिग्रहण किया ताकि हम शुरू से अंत तक पूरी थोक आदेश प्रक्रिया को मानकीकृत और बढ़ा सकें । लब्बोलुआब यह है, मेरा लक्ष्य है और हमेशा एक सकारात्मक ग्राहक अनुभव बनाना होगा । 



 



आपने एकमात्र पर काम करने का फैसला क्यों कियाधीरे?

मैंने बंधक व्यवसाय में एक लंबा करियर छोड़ा और ऐसे समय में स्थिरता की तलाश में था जब अर्थव्यवस्था और आवास बाजार बहुत अस्थिर थे । सोलेथ्रेड्स ने मुझे एक ऐसी जगह दी जहां मैं पेशेवर रूप से विकसित होने के लिए अपने कौशल सेट का उपयोग कर सकता था । जब सोलेथ्रेड्स(पूर्व में वासिली) अपनी प्रारंभिक अवस्था में था, तब आ रहा था, आदर्श था और मुझे वर्षों के माध्यम से अनुभव की गई घातीय वृद्धि को फिट करने के लिए एक आंतरिक टीम विकसित करने का मौका दिया । 



एक कंपनी के रूप में हमने जो पहला अभ्यास किया, वह हमारे मूल मूल्यों की पहचान करना था । एक दीवार पर प्रबंधन पलस्तर मूल्यों के बजाय, उन्होंने कर्मचारियों को यह पहचानने का अधिकार दिया कि हमारे लिए कौन से मूल मूल्य महत्वपूर्ण थे । अंत में इसका मतलब था कि हमने अपने मूल्यों के प्रति खुद को जवाबदेह ठहराया! मैं तब से जानता था कि टीम के निर्माण के अनुभव सोलेथ्रेड एक करियर होगा । 



 



आपको क्या लगता है कि एकमात्र में आपकी सबसे बड़ी उपलब्धि रही हैधीरे?

मेरी सबसे बड़ी उपलब्धि मेरी कस्टमर केयर टीम है । जब मैं 2011 में ग्राहक सहायता पर्यवेक्षक था, तो मेरे पास टीम के तीन सदस्य थे । वर्षों से, मेरी टीम एक आत्मनिर्भर विभाग के रूप में विकसित हो रही है । ग्राउंड अप से प्रशिक्षण कर्मचारियों ने उन्हें सोलेथ्रेड्स के अपने "टुकड़े" के मालिक होने में एक प्रेरित हिस्सेदारी लेने में सक्षम बनाया है । मुझे यह साझा करते हुए गर्व हो रहा है कि मेरे कई पूर्व टीम के सदस्यों को व्यावसायिक उद्यम के अन्य हिस्सों में पदोन्नत किया गया है और सोलेथ्रेड्स के साथ अपने शुरुआती कार्यकाल को करियर बनाया है । 



 



सोलेथ्रेड महिलाओं को कैसे सशक्त बनाता है?

पेशेवर रूप से, सोलेथ्रेड्स उन महिलाओं को सशक्त बनाता है जो केवल "9 से 5" नौकरी से अधिक कुछ खोजने की कोशिश कर रही हैं । हाथों पर प्रशिक्षण और संस्कृति सफलता के लिए एक आदर्श नुस्खा होने के लिए सत्यापित किया गया है । कार्य वातावरण तेजी से पुस्तक अभी तक पुरस्कृत है । कई व्यक्तियों ने एक ही में भाग लियाधीरे और इसे सामान्य रूप से सिर्फ एक नौकरी के बजाय एक कैरियर बनाया । 



एक उपभोक्ता दृष्टिकोण से, सोलेथ्रेड्स महिलाओं को अपने पैरों पर लाने का अधिकार देता है! समय की एक बहुत, व्यक्तियों के लिए चारों ओर चलने की क्षमता दी ले, दर्द से मुक्त. सोलेथ्रेड्स ने लाखों लड़कियों को स्टाइलिश दिखने के दौरान चलने और फिर से रहने में मदद की है । 



 



एक टीम पर काम करते समय आपके लिए 3 चीजें क्या महत्वपूर्ण हैं?

संचार, प्रशंसा और नेतृत्व।



 



कार्यबल में शुरू होने वाली युवा महिलाओं के लिए आपके पास क्या सलाह है?

कड़ी मेहनत करें, अपनी टीम का समर्थन करें, भूखे रहें । आपकी सफलता आएगी। ओह, और नोट ले लो!







आप सोलेथ्रेड हेडिंग का भविष्य कहां देखते हैं?

आकाश की सीमा है । जब मैं 7 साल पहले चला गया, तो मैं इस तरह की सफलता की कल्पना नहीं कर सकता था । लोगों के जीवन को बदलना लगातार सबसे आगे होगा कि हम एक ही समय में क्या करते हैं । 



 



क्या आपके पास एक पसंदीदा सोलेथ्रेड शैली है?

हमारे सतीमा स्नीकर और टाइड द्वितीय चप्पल ठोस शैली हैं जिन्होंने समय की परीक्षा आयोजित की है । मैं इन पर ट्रक वापस होगा, सप्ताह के किसी भी दिन. (यह एक शिपिंग मजाक है!)



अपने सबसे अच्छे आत्म महसूस करने के लिए अपने रहस्य क्या है?

सो जाओ! मैं सोल्थ्रेड्स और ए मॉम में एक निर्देशक हूं, और बहुत सारे कार्य हैं जो उन शीर्षकों के साथ आते हैं । मुझे अक्सर ऐसा लगता है कि मैं "बाद में सो सकता हूं", लेकिन यह अंततः आपके साथ पकड़ लेगा । मुझे ध्यान करने और पर्याप्त नींद लेने के लिए अपने लिए समय निकालना चाहिए । मैं अपनी टीम और अपनी बेटियों के लिए खुद का एक उन्नत संस्करण हूं । 